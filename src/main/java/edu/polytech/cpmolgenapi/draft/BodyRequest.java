package edu.polytech.cpmolgenapi.draft;

public class BodyRequest {
    private int[] degreeSequence;

    // Getter and setter for degreeSequence

    public int[] getDegreeSequence() {
        return degreeSequence;
    }

    public void setDegreeSequence(int[] degreeSequence) {
        this.degreeSequence = degreeSequence;
    }
}
